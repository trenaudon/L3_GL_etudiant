cmake_minimum_required (VERSION 2.8)

project( sayHello )

set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_OPENCV REQUIRED opencv )
include_directories( ${PKG_OPENCV_INCLUDE_DIRS} )

add_executable( imshow.out src/imshow.cpp)
target_link_libraries( imshow.out ${PKG_OPENCV_LIBRARIES})