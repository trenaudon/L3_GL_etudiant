#!/bin/sh

# check command line args
if [ $# -lt 1 ] ; then
	echo "usage: $0 <file name>"
	exit
fi

for name in "$@" ; do
	echo -n "*** "
	echo -n "$name"
	echo -n " ***"
	echo
	cat $name
	echo 
done
