#!/bin/sh

# check command line args
if [ $# -ne 2 ] ; then
	echo "usage: $0 <url> <output directory>";
	echo "example: $0 \"http://wimereux.biz/bigwebcam.htm\" output_jpg";
	exit
fi

if [ ! -d "$2" ] ; then
	mkdir "$2" ;
else 
	rm -r "$2"/;
fi

wget -r --accept "*.jpg" --no-host-directories --directory-prefix="$2" "$1";
