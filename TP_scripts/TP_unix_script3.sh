#!/bin/bash

touch fibo.csv

for i in `seq 42` ; do
	echo `./fibo.out $i | tail -n 1` >> fibo.csv
done
