#!/bin/sh

# check command line args
if [ $# -ne 2 ] ; then
	echo "usage: $0 <nb loops> <output directory>";
	echo "example: $0 \"http://wimereux.biz/bigwebcam.htm\" output_jpg";
	exit
fi

if [ ! -d "$2" ] ; then
	mkdir "$2" ;
else 
	rm -r "$2"/;
fi

for i in `seq 1 "$1"` ; do
	wget -r --accept "Photo.jpg" --no-host-directories --directory-prefix="$2" http://wimereux.biz/bigwebcam.htm;
	mv "$2"/Webcam_1/Photo.jpg "$2"/Webcam_1/Photo_$i.jpg
	if [ $i -lt "$1" ] ; then
		sleep 2m;
	fi
done

convert -delay 20 -loop 0 "$2"/Webcam_1/*.jpg "$2"/webcam.gif;
